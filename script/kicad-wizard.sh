#!/bin/bash
set -e  # exit on error

echo 
echo "--------------------------------------------------------------------------------"
echo " Kicad Repo Wizard"
echo "--------------------------------------------------------------------------------"
echo

echo "Where should the project be stored? I will create the project directory there."
read path
echo
cd $path

echo "Whats the name of the project?"
read name
echo
mkdir $name
cd $name

echo "Who is the author?"
read author
echo

echo "Add offical kicad libs as submodule? y/n"
read submodule
echo

# Template repo hardcoded
repoName="kicad-template-repo"
branch="master"
repoURL="https://gitlab.com/christoph.zaugg/$repoName/-/archive/$branch/$repoName-$branch.zip"
gitignoreURL="https://gitlab.com/christoph.zaugg/$repoName/-/raw/$branch/.gitignore"

echo
echo "--------------------------------------------------------------------------------"
echo " Download and edit template"
echo "--------------------------------------------------------------------------------"
echo

# Download and unzip template
# We get the repo int a archive.zip
# The actual repo content is archive.zip/repo-name-branch/**
echo "Download template"
curl -s $repoURL -L --output archive.zip
curl -s $gitignoreURL -L --output .gitignore
echo "Unzip template"
unzip archive.zip

# Copy all files into project root
echo "Copy files into root"
cp -r $repoName-$branch/* .

# Remove .zip and unzipped files
echo "Cleanup files"
rm -r $repoName-$branch
rm archive.zip
rm -r script

# Update repo name in filenames
echo
echo "Update file names to $name"
find -iname $repoName*
find -iname $repoName* -execdir rename "s/$repoName/$name/g" {} \;

# Update names and dates in doc files
year=$(date +'%Y')
date=$(date +'%d. %B %Y')

echo
echo "Update \$project-name to $name"
grep -ri "\$project-name" * 
grep -rli "\$project-name" * | xargs -i@ sed -i "s/\$project-name/$name/g" @

echo
echo "Update \$current-year to $year"
grep -ri "\$current-year" * 
grep -rli "\$current-year" * | xargs -i@ sed -i "s/\$current-year/$year/g" @

echo
echo "Update \$project-author to $author"
grep -ri "\$project-author" * 
grep -rli "\$project-author" * | xargs -i@ sed -i "s/\$project-author/$author/g" @

echo
echo "Update \$creation-date to $date"
grep -ri "\$creation-date" * 
grep -rli "\$creation-date"  * | xargs -i@ sed -i "s/\$creation-date/$date/g" @

echo
echo "--------------------------------------------------------------------------------"
echo " Setup repo and submodules"
echo "--------------------------------------------------------------------------------"
echo

# Setup repo and get parts libraries as submodules
echo "Setup git repo"
git init

if [[ $submodule == *"y"* ]]; then
    echo "Setup submodules"
    rm -r pcb/lib/* # git will not clone into existing folders
    git submodule add https://gitlab.com/kicad/libraries/kicad-symbols.git pcb/lib/official-symbols 
    git submodule add https://gitlab.com/kicad/libraries/kicad-footprints.git pcb/lib/official-footprints
else
    echo "Skipping submodules"
fi

git add * 
git add .gitignore 
git commit -m "Setup repo."
git checkout -b develop 

explorer.exe .

echo
echo "--------------------------------------------------------------------------------"
echo " Repo created"
echo "--------------------------------------------------------------------------------"
echo
