# KiCad Wizard

This is a script that sets up a new Kicad project with Git version control.

1. Copies files from template project
2. Fills in names and dates
3. Inits git repo
4. Clones part libraries as submodules
5. Commits setup project and switches to develop

Note: You need to add a remote manually

## Usage

Open Terminal (WSL on Windows)

`kicad-template-repo/script/kicad-wizard.sh`

The wizard will ask you some questions and then setup the project files.
Before finishing it opens the folder with the project in Windows Explorer.

## Dependencies

The script uses following programs, make sure they are installed:

- curl
- unzip
- git
- rename

## Template variables

The template uses following variables:

- $project-name
- $project-author
- $creation-date

The kicad-wizard.sh script updates all occurrences of these strings in any file.
Additionally, all filenames that match the repositories name are updated to the project name.

***(c) 2023 by CAZaugg***
