# Readme of $project-name PCB

## Project description

This is where your PCB description goes. If you want to learn more about this template look at [kicad-wizard]([script/kicad-wizard.md](https://gitlab.com/christoph.zaugg/kicad-template-repo/-/blob/master/script/kicad-wizard.md)

## Workflow and project organization

- [PCB design and part libraries](pcb/pcb-info.md)
- [Documentation](pcb/doc-info.md)
- [Production](pcb/prod-info.md)

## Tools

Made with KiCad 7.

***(c) $current-year by $project-author***
